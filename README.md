# Ansible deployment for openqa.gnome.org

This repo is used to deploy the OpenQA web UI at https://openqa.gnome.org/.

It should be safe to run the full playbook against the server, but please be careful -
use the `--check` mode to confirm what will change, before making any changes.

The following variables must be set when running the playbook. (Set these with `-e`,
e.g.: `-e openqa_needles_token=12346789`):

  * `openqa_needles_token`: a Gitlab [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) for the GNOME/openqa_needles repo, allowing the web UI to push needle updates. Maintainers can create these [here](https://gitlab.gnome.org/GNOME/openqa-needles/-/settings/access_tokens). The token scope should be `write_repository`.
  * `openqa_oauth2_secret`: access token for the GNOME OAuth2 identity provider. This is inserted into [openqa.ini](roles/openqa/templates/openqa.ini). To get this code, look in the deployed `openqa.ini` file on openqa.gnome.org, or contact the [GNOME Infrastructure Team](https://wiki.gnome.org/Infrastructure).

Note that not all deployment steps are automated yet: see
https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/openqa/Deployment for
extra manual steps if deploying from scratch.
